<?php

class School extends DataObject implements RegistryDataInterface {
	static $db = array(
		'SchoolID' => 'Int',
		'Name' => 'Varchar(255)',
		'Telephone' => 'Varchar(50)',
		'Fax' => 'Varchar(50)',
		'Email' => 'Varchar(255)',
		'Principal' => 'Varchar(255)',
		'Website' => 'Varchar(255)',
		'Street' => 'Varchar(255)',
		'Suburb' => 'Varchar(50)',
		'City' => 'Varchar(50)',
		'Type' => 'Enum("Composite (Year 1-15), Contributing, Correspondence School, Full Primary, Intermediate, na, Restricted Composite (Year 7-10), Secondary (Year 7-15), Secondary (Year 9-15), Special School, Teen Parent Unit")',
		'Gender' => 'Enum("Boys School, Boys/Senior Co-Ed., Co-Educational, Girls School, Primary Co-ed, Secondary Girls")',
		'MinEdLocalOffice' => 'Enum("Auckland North, Auckland South, Christchurch, Dunedin, Hamilton, Invercargill, Napier, Nelson, Rotorua, Wellington, Whanganui")',
		'EducationRegion' => 'Enum("Central North, Central South, Northern, Southern")',
		'Longitude' => 'Varchar(50)',
		'Latitude' => 'Varchar(50)',
		'Decile' => 'Int',
		'RollTotal' => 'Int',
		'RollPakeha' => 'Int',
		'RollMaori' => 'Int',
		'RollPasifika' => 'Int',
		'RollAsian' => 'Int',
		'RollMELAA' => 'Int',
		'RollOther' => 'Int',
		'RollInternational' => 'Int'
	);

	static $summary_fields = array(
		'Name',
		'City'
	);

	public function getSearchFields() {
		$fields = new FieldList(
			new TextField('Name'),
			new TextField('Suburb'),
			new TextField('City'),
			$regionDropdown = new DropdownField('EducationRegion', 'Region', singleton('School')->dbObject('EducationRegion')->enumValues()),
			$typeDropdown = new DropdownField('Type', 'Type', singleton('School')->dbObject('Type')->enumValues()),
			$genderDropdown = new DropdownField('Gender', 'Gender', singleton('School')->dbObject('Gender')->enumValues()),
			new TextField('Decile')
		);

		$regionDropdown->setHasEmptyDefault(true);
		$typeDropdown->setHasEmptyDefault(true);
		$genderDropdown->setHasEmptyDefault(true);

		return $fields;
	}

	public function Link($action = 'show') {
		$page = RegistryPage::get()->filter('DataClass', get_class($this))->First();
		return Controller::join_links($page->Link(), $action, $this->ID);
	}
}